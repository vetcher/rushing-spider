package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/vetcher/rushing-spider/pkg/manager"
	"gitlab.com/vetcher/rushing-spider/pkg/requester"
	"golang.org/x/sync/errgroup"
)

var (
	flagRateLimit   = flag.Duration("rate", time.Millisecond*500, "Limit requests rate. Should be in time.Duration string format.")
	flagDomainLevel = flag.Int("domain-level", 0, "Value represents how many domain levels of new url should be equal to be requested. Infinite (hosts should be strong equal) by default.")
)

func init() {
	flag.Parse()
}

func main() {
	if len(os.Args) == 1 {
		fmt.Fprintln(os.Stderr, "root url is empty")
		os.Exit(1)
	}
	root := os.Args[len(os.Args)-1]
	rootUrl, err := url.Parse(root)
	if err != nil {
		fmt.Fprintf(os.Stderr, "cannot parse root url: %v", err)
		os.Exit(1)
	}
	httpClient := &http.Client{
		Timeout: time.Second * 10,
	}
	requester := requester.NewCycleBreaker()(
		requester.NewLimiter(*flagRateLimit)(
			requester.NewHTTPRequester(httpClient),
		),
	)

	g, ctx := errgroup.WithContext(context.Background())
	g.Go(sigHandler(ctx))

	ch := make(chan string, 1000)
	ch <- rootUrl.String()

	hostEqualCheck := manager.EqualHost(rootUrl.Host)
	if *flagDomainLevel > 0 {
		hostEqualCheck = manager.LevelDomain(rootUrl.Host, *flagDomainLevel)
	}
	m := manager.NewManager(hostEqualCheck, 10)

	g.Go(worker(m, requester, ch))

	if err := g.Wait(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

// sigHandler handles first SIGINT and SIGTERM and returns it as error
func sigHandler(ctx context.Context) func() error {
	return func() error {
		interruptHandler := make(chan os.Signal, 1)
		signal.Notify(interruptHandler, syscall.SIGINT, syscall.SIGTERM)
		select {
		case sig := <-interruptHandler:
			return errors.Errorf("signal received: %s", sig.String())
		case <-ctx.Done():
			return errors.Wrap(ctx.Err(), "signal listener")
		}
	}
}

func worker(m *manager.Manager, requester requester.Interface, urlsCh chan string) func() error {
	return func() error {
		// we should choose: stop fast or grace
		// here we choose to stop fast
		go m.Run(requester, urlsCh)
		return nil
	}
}
