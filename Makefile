dep-docker: ; GO111MODULES=on go mod download

lint: ; golangci-lint run --no-config --deadline=30m --disable-all -E govet -E megacheck -E varcheck -E structcheck -E deadcode -E ineffassign

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o rushing-spider ./cmd/rushing-spider/main.go