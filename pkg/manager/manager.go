package manager

import (
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
	"sync"
	"sync/atomic"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"gitlab.com/vetcher/rushing-spider/pkg/requester"
)

// Manager manage core spider workflow, such as
// request -> parse -> print -> repeat
type Manager struct {
	founded       map[string]struct{}
	mx            sync.Mutex
	counter       int64
	isSameHost    func(string) bool
	workersAmount int
}

func NewManager(isSameHost func(string) bool, workersAmount int) *Manager {
	return &Manager{isSameHost: isSameHost, workersAmount: workersAmount, founded: make(map[string]struct{}, 1000)}
}

func (m *Manager) Run(requester requester.Interface, urls chan string) {
	m.workerPool(requester, urls, m.workersAmount)
}

func (m *Manager) workerPool(requester requester.Interface, urls chan string, amount int) {
	var wg sync.WaitGroup
	wg.Add(amount)
	for range make([]struct{}, amount) {
		go func() {
			defer wg.Done()
			m.work(requester, urls)
		}()
	}
	wg.Wait()
}

func (m *Manager) work(requester requester.Interface, urls chan string) {
	for u := range urls {
		contentReader, err := requester.Get(u)
		if err != nil {
			fmt.Fprintf(os.Stderr, "get %s: %v\n", u, err)
			continue
		}
		foundedLinks, err := findOnPage(contentReader)
		if err != nil {
			fmt.Fprintf(os.Stderr, "not found links on page %s: %v\n", u, err)
			continue
		}
		foundedLinks = m.filterByHost(foundedLinks)
		// all workers can be deadlocked when buffer would be full, so
		// run concurrently
		go m.putFoundedToChan(foundedLinks, urls)
		// run concurrently because we want use "all resources"
		go m.printFounded(u, foundedLinks)
	}
}

func (m *Manager) filterByHost(links []string) []string {
	for i := 0; i < len(links); {
		parsedURL, err := url.Parse(links[i])
		if err != nil {
			links = removeFromSlice(links, i)
			continue
		}
		if !m.isSameHost(parsedURL.Host) {
			links = removeFromSlice(links, i)
			continue
		}
		i++
	}
	return links
}

func removeFromSlice(from []string, index int) []string {
	from[index] = from[len(from)-1]
	return from[:len(from)-1]
}

func findOnPage(reader io.ReadCloser) ([]string, error) {
	doc, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse html doc")
	}
	if err := reader.Close(); err != nil {
		// just report error,
		// it does not break workflow
		fmt.Fprintln(os.Stderr, "close reader: ", err)
	}
	const averageLinksOnPage = 30 // just a random number
	links := make([]string, 0, averageLinksOnPage)
	doc.Find("a").Each(func(_ int, sel *goquery.Selection) {
		link, ok := sel.Attr("href")
		if !ok {
			return
		}
		links = append(links, link)
	})
	return links, nil
}

func (m *Manager) printFounded(where string, foundedUrls []string) {
	for i := range foundedUrls {
		fmt.Println(atomic.AddInt64(&m.counter, 1), where, ":", foundedUrls[i])
	}
}

func (m *Manager) putFoundedToChan(foundedURLs []string, ch chan string) {
	m.mx.Lock()
	defer m.mx.Unlock()
	for i := range foundedURLs {
		if _, ok := m.founded[foundedURLs[i]]; ok {
			continue
		}
		m.founded[foundedURLs[i]] = struct{}{}
		ch <- foundedURLs[i]
	}
}

func EqualHost(origin string) func(string) bool {
	return func(h string) bool {
		return h == origin
	}
}

func LevelDomain(origin string, level int) func(string) bool {
	l := 0
	idx := strings.LastIndexFunc(origin, func(r rune) bool {
		if r == '.' {
			l++
		}
		return l == level
	})
	suffix := origin[idx:]
	return func(h string) bool {
		return strings.HasSuffix(h, suffix)
	}
}
