package requester

import (
	"io"
	"net/http"
	"sync"
	"time"

	"github.com/pkg/errors"
)

// Interface is a core type, that allows to get content by url
type Interface interface {
	Get(url string) (io.ReadCloser, error)
}

type httpRequester struct {
	client *http.Client
}

func NewHTTPRequester(client *http.Client) *httpRequester {
	return &httpRequester{client: client}
}

func (r *httpRequester) Get(url string) (io.ReadCloser, error) {
	resp, err := r.client.Get(url)
	if err != nil {
		return nil, errors.Wrap(err, "get")
	}
	return resp.Body, nil
}

type Limiter struct {
	ticker <-chan time.Time
	next   Interface
}

func NewLimiter(every time.Duration) func(Interface) *Limiter {
	return func(next Interface) *Limiter {
		return &Limiter{
			//nolint
			// we ignore linter here because
			// Limiter lives until app shutdown.
			ticker: time.Tick(every),
			next:   next,
		}
	}
}

func (l *Limiter) Get(url string) (io.ReadCloser, error) {
	<-l.ticker
	return l.next.Get(url)
}

type CycleBreaker struct {
	mx        sync.Mutex
	requested map[string]struct{}
	next      Interface
}

var ErrAlreadyRequested = errors.New("already requested")

func NewCycleBreaker() func(Interface) *CycleBreaker {
	return func(next Interface) *CycleBreaker {
		return &CycleBreaker{requested: make(map[string]struct{}), next: next}
	}
}

func (c *CycleBreaker) Get(url string) (io.ReadCloser, error) {
	c.mx.Lock()
	defer c.mx.Unlock()
	if _, ok := c.requested[url]; ok {
		return nil, ErrAlreadyRequested
	}
	c.requested[url] = struct{}{}
	return c.next.Get(url)
}
