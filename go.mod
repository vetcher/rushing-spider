module gitlab.com/vetcher/rushing-spider

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/pkg/errors v0.8.1
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6
)
