FROM golang:1.12 AS initial

ADD . /vgo
WORKDIR /vgo
RUN make dep-docker build

FROM alpine:latest

ENV TZ=Europe/Moscow
RUN apk --no-cache add ca-certificates tzdata && cp -r -f /usr/share/zoneinfo/$TZ /etc/localtime

COPY --from=initial /vgo/rushing-spider ./rushing-spider
LABEL description="$CI_PROJECT_NAME"

ENTRYPOINT ["/rushing-spider"]
EXPOSE 9000 9001